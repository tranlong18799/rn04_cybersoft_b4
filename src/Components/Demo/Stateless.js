import React from 'react'
import { Text, View } from 'react-native'

function Stateless() {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ fontSize: 24, fontWeight: '700', color: 'black' }}>
                Hello React Native Application Stateless
            </Text>
        </View>
    )
}

export default Stateless;