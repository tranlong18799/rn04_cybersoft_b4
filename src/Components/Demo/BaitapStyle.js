import React from 'react'
import { StyleSheet, Text, View } from 'react-native';

const style = StyleSheet.create({
    container: {
        flex:1,
        paddingVertical: 50,
        paddingHorizontal:15,
        
    },
    box:{
        flex:1,
        backgroundColor:'#C9C9FF',
        marginVertical:10,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:10
    },
    blue:{
        backgroundColor:'blue'
    },
    green:{
        backgroundColor:'green'
    },
    yellow:{
        backgroundColor:'yellow'
    },
    infoTxt:{
        fontSize:26,
        fontWeight:'800'
    }
})
export default function BaitapStyle() {
    return (
        // <View style={{ flex: 1, justifyContent: 'center', backgroundColor: 'gray' }}>
        //     <View style={{ flex: 1, backgroundColor: 'green', marginHorizontal: 10, marginVertical: 10, borderRadius: 10 }}>
        //         <Text style={{ flex: 1, alignSelf: 'center', }}>GREEN</Text>
        //     </View>
        //     <View style={{ flex: 1, backgroundColor: 'black', marginHorizontal: 10, marginVertical: 10, borderRadius: 10 }}>
        //         <Text style={{ flex: 1, alignSelf: 'center', color: 'white' }}>BLACK</Text>
        //     </View>
        //     <View style={{ flex: 1, backgroundColor: 'pink', marginHorizontal: 10, marginVertical: 10, borderRadius: 10 }}>
        //         <Text style={{ flex: 1, alignSelf: 'center' }}>PINK</Text>
        //     </View>
        //     <View style={{ flex: 1, backgroundColor: 'blue', marginHorizontal: 10, marginVertical: 10, borderRadius: 10 }}>
        //         <Text style={{ flex: 1, alignSelf: 'center' }}>BLUE</Text>
        //     </View>
        // </View>
        <View style={style.container}>
            <View style={[style.box,style.blue]}><Text>A</Text></View>
            <View style={[style.box,style.green]}><Text>B</Text></View>
            <View style={[style.box,style.yellow]}><Text>C</Text></View>
            <View style={[style.box]}><Text>D</Text></View>
        </View>
    )
}
