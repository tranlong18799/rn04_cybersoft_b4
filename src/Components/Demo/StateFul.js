import React, { Component } from 'react'
import { View,Text } from 'react-native'

class StateFul extends Component {
    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontSize: 24, fontWeight: '700', color: 'black' }}>
                    Hello React Native Application StateFull
                </Text>
            </View>
        )
    }
}

export default StateFul;
