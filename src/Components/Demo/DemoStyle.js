import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'

const style = StyleSheet.create({
    box: {
        backgroundColor: '#bfb',
        width: 100,
        height: 100,
        //  margin:20,
        marginVertical: 20,
        marginHorizontal: 10,
        borderWidth: 2,
        borderStyle: 'dashed',
        borderColor: 'green'
    }
})

export default class DemoStyle extends Component {
    render() {
        return (
            <View style={{ flex: 1, paddingTop: 50, paddingBottom: 10 }}>
                <Text>Demo Style Component</Text>
                <View style={{
                    flex: 1,
                    // flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    // flexWrap:'wrap'

                }}>
                    <View style={{
                        ...style.box,
                        backgroundColor: '#bbf',
                        // alignSelf:'flex-end'
                    }} />
                    <View style={{ flexDirection: 'row' }}>
                        <View style={style.box} />
                        <View style={{ ...style.box, backgroundColor: '#fbf' }} />
                    </View>
                    <View style={{ ...style.box, backgroundColor: '#bfb' }} />
                    <View style={{ ...style.box, backgroundColor: '#000' }} />
                </View>
            </View>
        )
    }
}
