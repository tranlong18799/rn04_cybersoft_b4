import React, { Component } from 'react'
import { Image, Text, View } from 'react-native'
// import cat from '../cat.jpg';

export default class DemoImage extends Component {
    render() {
        return (
            <View style={{flex:1, justifyContent:'center',alignItems:'center'}}>
                <Text>
                    DEMO IMAGE COMPONENT
                </Text>
                {/* Internal Image */}
                {/* <Image source={require('./cat.jpg')}/> */}
                {/* <Image source={cat} /> */}
                {/* External Image phai co them style để hiển thị được hình*/}
                <Image source={{
                    uri:'https://i.pinimg.com/736x/02/d1/8d/02d18dda75a869b005522046f5aa245b.jpg'
                }}
                style={{width:200,height:100}}
                />
            </View>
        )
    }
}
