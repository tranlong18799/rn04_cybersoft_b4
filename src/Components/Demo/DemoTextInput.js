import React, { Component } from 'react'
import { Text, TextInput, View } from 'react-native'

export default class DemoTextInput extends Component {
    render() {
        return (
            <View style={{flex:1, justifyContent:'center',alignItems:'center'}}>
                <Text>Demo Text Input</Text>
                <TextInput style={{width:'70%', backgroundColor:'#eee',fontSize:30,
                borderWidth:1, borderRadius:8
            }} />
            </View>
        )
    }
} 
