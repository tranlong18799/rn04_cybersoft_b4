import React, { Component } from 'react'
import { View } from 'react-native';
import {style} from './style';

const {container,wrapper,PaleGoldenrod,roundedBottom,Tan4,boxFull,shadow,border,flexRow,roundedStart,
    boxHalf
    } = style;
    const boxCol = {...wrapper,...PaleGoldenrod,...roundedBottom};
    const boxColChild = {...Tan4, ...boxFull, ...shadow, ...border};
    const boxRow ={...wrapper, ...Tan4, ...flexRow, ...roundedStart};
    const boxRowChild ={...boxHalf, ...PaleGoldenrod, ...shadow, ...border};
    
export default class BaitapComponent extends Component{
    render() {
        
        return (
            <View style={container}>
                <View style={boxCol} >
                    <View style={boxColChild} />
                    <View style={boxColChild} />
                </View>
                <View style={boxRow}>
                    <View style={boxRowChild} />
                    <View style={boxRowChild} />
                    <View style={boxRowChild} />
                    <View style={boxRowChild} />
                </View>
            </View>
        )
    }
}
