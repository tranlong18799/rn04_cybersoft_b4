import { StyleSheet } from "react-native";

export const style = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'black'
    },
    wrapper: {
        flex: 1,
    },
    flexRow: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    shadow: {
        shadowColor: 'black',
        shadowOpacity: 10,
    },
    border: {
        borderWidth: 1,
        borderColor: 'gray',
        opacity: 0.3
    },
    boxFull: {
        flex: 1,
        marginHorizontal: 15,
        marginVertical: 40,
        borderRadius: 20
    },
    boxHalf: {
        marginHorizontal: 20,
        marginTop: 30,
        borderRadius: 20,
        width: 150,
        height: 150,
    },
    roundedBottom: {
        marginBottom: 10,
        borderBottomEndRadius: 20,
        borderBottomStartRadius: 20
    },
    roundedStart: {
        marginTop: 10,
        borderTopEndRadius: 20,
        borderTopStartRadius: 20
    },
    green: {
        backgroundColor: 'green'
    },
    blue: {
        backgroundColor: 'blue'
    },
    yellow: {
        backgroundColor: 'yellow',
    },
    PaleGoldenrod: {
        backgroundColor: '#EEE8AA'
    },
    white: {
        backgroundColor: 'white'
    },
    Tan4: {
        backgroundColor: '#8B5A2B'
    },
})